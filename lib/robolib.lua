local component = require("component")
local event = require("event")
local modem = component.modem

local robolib = {}

local run, execute, string_to_boolean, unserialize

run = function(cmd)
  modem.broadcast(2412, cmd)
  return(select(6, event.pull(5, "modem_message")))
end

-- Make version that doesnt wait for event????
-- May need to clear event queue before pushing in that case?
execute = function(cmd)
  modem.broadcast(2412, "return " .. cmd)
  local unserialized = unserialize(select(6, event.pull(5, "modem_message")))
  return table.unpack(unserialized)
end

string_to_boolean = function(input)
  if input == "true" then 
    return true 
  elseif input == "false" then 
    return false 
  else
    return nil
  end
end

unserialize = function(serialized)
  serialized = serialized .. "%"
  local unserialized = {}
  local currentVal = ""
  local valType = "_start"

  for c in serialized:gmatch"." do
    if c == "%" then
      if not (valType == "_start") then
        local converted
        if valType == "n" then converted = nil
        elseif valType == "b" then converted = string_to_boolean(currentVal)
        elseif valType == "i" then converted = tonumber(currentVal)
        elseif valType == "s" then converted = currentVal
        else converted = "ERR CONVERTING VAL" end
        table.insert(unserialized, converted)
        currentVal = ""
      end
      valType = "_found"

    else if valType == "_found" then
      valType = c

    else
      currentVal = currentVal .. c end
    end
  end

  return unserialized
end

function robolib.init()
  modem.open(2412)
  modem.broadcast(2412, "robot=component.proxy(component.list('robot')())")
  event.pull(5, "modem_message")
end

function robolib.run(cmd)
  return(run(cmd))
end

function robolib.execute(cmd)
  return(execute(cmd))
end

function robolib.unserialize(cmd)
  return(unserialize(cmd))
end

-- Basic assorted
function robolib.durability()
  return(execute("robot.durability()"))
end

function robolib.name()
  return(execute("robot.name()"))
end

-- Basic movement
-- TODO: Test sending entire for loops and such for increased speed
function robolib.moveForward(x)
  return(execute("robot.move(3)"))
end

function robolib.moveBackward(x)
  return(execute("robot.move(2)"))
end

function robolib.moveUp(x)
  return(execute("robot.move(1)"))
end

function robolib.moveDown(x)
  return(execute("robot.move(0)"))
end

-- Basic rotation
function robolib.turnRight()
  return(execute("robot.turn(true)"))
end

function robolib.turnLeft()
  return(execute("robot.turn(false)"))
end

function robolib.turnAround()
  return execute("robot.turn(true)"), execute("robot.turn(true)")
end

function robolib.detect(x) 
  return execute("robot.detect(" .. x .. ")")
end

return robolib
