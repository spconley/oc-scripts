package.loaded.robolib = nil
local r = require("robolib")

r.init()

-- Test simplified movements
r.moveUp()
r.moveUp()
r.moveUp()

r.moveForward()
r.moveForward()
r.moveForward()

r.moveDown()
r.moveDown()
r.moveDown()

r.moveBackward()
r.moveBackward()
r.moveBackward()

-- Test passthrough movements
r.execute("robot.move(1)")
r.execute("robot.move(0)")

-- Test simplified turns
r.turnRight()
r.turnLeft()
r.turnAround()
r.turnAround()

-- Misc
print("Name: " .. r.name())
print("Durability: " .. tostring(r.durability()))
print("Block directly below:")
print(r.detect(0))
