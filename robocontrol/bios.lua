local m=component.proxy(component.list("modem")())

m.open(2412)

local function serialize(...)
  local args=table.pack(...)
  local serialized = ""

  for i,arg in ipairs(args) do
      local ident = "%"

      if type(arg) == "nil" then ident = ident .. "n"
      elseif type(arg) == "boolean" then ident = ident .. "b"
      elseif type(arg) == "number" then ident = ident .. "i"
      elseif type(arg) == "string" then ident = ident .. "s"
      else ident = ident .. "e" 
      end

      serialized = serialized .. ident .. tostring(arg)
  end

  return serialized
end

local function respond(...)
  serialized = serialize(...)
  pcall(function() m.broadcast(2412, serialized) end)
end

local function receive()
  while true do
    local evt,_,_,_,_,cmd=computer.pullSignal()
    if evt=="modem_message" 
    then
      return load(cmd) 
    end
  end
end

while true do
  local result,reason=pcall(function()
    local result,reason=receive()
    if not result then return respond(reason) end
    respond(result())
  end)
  if not result then respond(reason) end
end
