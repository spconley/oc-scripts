local baseurl = "https://gitlab.com/spconley/oc-scripts/-/raw/master/"

-- Get robocontrol
print('Update robocontrol? [Y/y]')
local prompt = io.read()
if prompt == "Y" or prompt == "y"
then
  local dir = "robocontrol/"
  local files = {"bios.lua", "server.lua", "debug.lua"}
  os.execute("mkdir " .. dir)
  for i,file in ipairs(files) do
    local abPath = dir .. file
    os.execute("wget -f " .. baseurl .. abPath .. " " .. abPath)
  end
end

-- Get remote-dl
print('Update remote-dl? [Y/y]')
local prompt = io.read()
if prompt == "Y" or prompt == "y"
then
  local file = "misc/remote-dl.lua"
  os.execute("wget -f " .. baseurl .. file)
end

-- Get robolib
print('Update robolib? [Y/y]')
local prompt = io.read()
if prompt == "Y" or prompt == "y"
then
  local file = "lib/robolib.lua"
  os.execute("mkdir /usr/lib")
  os.execute("wget -f " .. baseurl .. file .. " " .. "/usr/" .. file)

  print('Reboot? [Y/y]')
  local prompt = io.read()
  if prompt == "Y" or prompt == "y" then os.execute("reboot") end
end
